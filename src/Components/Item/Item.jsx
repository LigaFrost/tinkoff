import React from "react";
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    },
    container: {
        padding:"5em",
        display:"flex",
        justifyContent:"center"
    }

});

export default function Item(items) {
    const classes = useStyles();
    return items.map((link, i) => ((

        <div className={classes.container}>
        <Card className={classes.root}>
            <CardActionArea>
                <CardMedia
                    className={classes.media}
                    image={link.imageURL}
                    title="Contemplative Reptile"
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="h2">
                        {link.name}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                        {link.price} руб.
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
        </div>
    )));
}

