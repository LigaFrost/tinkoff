import React from "react";
import TextField from '@material-ui/core/TextField';
// <div>{JSON.stringify(fieldsValues)}</div>
export default function Fields ({ id, value, handleChange, label }) {
    return(
    <div style={{padding:"1em"}}>
        <TextField onChange={event => handleChange(event, id)} value={value} required id="standard-required" label={label} />
    </div>
    );
}


