import React, {useCallback, useEffect, useState} from "react";
import Fields from "../Components/Fields";
import Payment from "../Components/Payment";
import Item from "../Components/Item";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles({
    root: {
        '&:hover': {
            backgroundColor: 'transparent',
        },
    },
    buyButton: {
        padding: "15px 31px",
        width:"55px",
        minHeight: "20px",
        fontSize: "15px",
        lineHeight: "24px",
        backgroundColor: "#c3c3c3",
        borderRadius: "4px",
        border: "1px solid transparent",
        cursor: "pointer",
        color: "#333",
    },
    container:{
        display:"flex",
        alignItems:"center",
        justifyContent:"center"
    }
});

function Checkout() {
    const classes = useStyles();

    // Текстовые поля
    const [fieldsValues, setFieldsValues] = React.useState({});
    const handleChange = (event, fieldId) => {
        let newFields = { ...fieldsValues };
        newFields[fieldId] = event.target.value;
        setFieldsValues(newFields);
    };
    const fieldForm = ()=>(
        <div style={{display:"flex", flexDirection:"row", justifyContent:"center"}}>
            {fields.map(field => (
                <Fields
                    key={field.name}
                    id={field.name}
                    handleChange={handleChange}
                    value={fieldsValues[field]}
                    label={field.label}
                />
            ))}
        </div>
    )
    const fields = [
        {
            "name":"surname",
            "label":"Фамилия"
        },
        {
            "name":"name",
            "label":"Имя"
        },
        {
            "name":"middleName",
            "label":"Отчество"
        },
        {
            "name":"phone",
            "label":"Телефон"
        },
        {
            "name":"email",
            "label":"E-mail"
        },
    ];

    //Товары
    const items = [
        {
            "name":"Iphone 12",
            "price":110000,
            "imageURL":"./iphone12.png",
        }
    ]
    const [loaded, setLoaded] = useState(false)
    useEffect(() => {
        if (!loaded && typeof tinkoff !== "undefined")
        {
            setLoaded(true);

        }
    })
    const onTinkoffClick = useCallback(() => {
        if (loaded) {
            console.log(fieldsValues);
            window.tinkoff.createDemo(
                {
                    sum: items[0].price,
                    items: [{name: JSON.stringify(items[0].name), price: items[0].price, quantity: 1}],
                    demoFlow: 'sms',
                    promoCode: 'default',
                    shopId: 'b4e94e41-61b3-4d29-a853-74c6ff734e93',
                    showcaseId: '32acfed3-1714-4695-bd23-9698c6a12c61',
                    values: {
                        contact: {
                            fio: {
                                lastName: fieldsValues.surname,
                                firstName: fieldsValues.name,
                                middleName: fieldsValues.middleName
                            },
                            mobilePhone: fieldsValues.phone,
                            email: fieldsValues.email
                        }
                    }
                }
            )
        }
    }, [loaded, fieldsValues])

    return (
        <>
            {Item(items)}
            {fieldForm()}
            <Payment/>
            <div className={classes.container}>
                <div className={classes.buyButton} onClick={()=> console.log(items,fieldsValues)}>Купить</div>
                <button className="TINKOFF_BTN_YELLOW TINKOFF_SIZE_L" style={{margin:"1em"}} onClick={onTinkoffClick}/>
            </div>


        </>
    );
}
export default Checkout;
